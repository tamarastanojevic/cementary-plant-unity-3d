﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;
using UnityEngine.Events;

public class Controller : MonoBehaviour
{
    //public static string url = "http://127.0.0.1:8080/Resources/ListaTacaka.json";
    public GameObject go;
    public GameObject ppoint;
    public GameObject coordinate;
    public GameObject container;
    public GameObject tooltip;
    public GameObject panelEdit;
    public Text pathText;
    public Button btnCancel;
    public Camera myCamera;
    public Text IDtext;
    public Text Nametext;
    public Text ParamText;
    public Text TextVal2;
    public Text TextVal3;
    public Text TextVal1;
    public Text TextVal2Preview;
    public Text TextVal3Preview;
    public Text TextVal1Preview;
    public Button btnReset;
    public Button btnEditMode;
    public Text Edittext;
    public GameObject plant1;
    public GameObject plant2;
    public InputField inputPar;
    public InputField inputName;
    public Button btnCancelEdit;
    public Button btnSaveEdit;
    public Button btnCancelWarning;
    public GameObject panelWarning;
    public GameObject LeftWallScreen;
    public GameObject TopLeft;
    public GameObject MiddleLeft;
    public GameObject BottomLeft;
    public GameObject MiddleRight;
    public GameObject Boundmin2Left;
    public GameObject Bound2Left;
    public GameObject Boundmin5Left;
    public GameObject Bound5Left;
    public GameObject Boundmin2Right;
    public GameObject Bound2Right;
    public GameObject Boundmin5Right;
    public GameObject Bound5Right;
    public Material material;
    public Button btnPreview;
    public Button btnBack;
    public GameObject panelPreview;
    public Text paramPreview;
    public Text val1Preview;
    public Text val2Preview;
    public Text val3Preview;
    public Text val1prev;
    public Text val2prev;
    public Text val3prev;

    private bool saveMode=false;
    private Color red = new Color(242f / 255f, 36f / 255f, 33f / 255f);
    private Color yellow = new Color(219f / 255f, 209f / 255f, 2f / 255f);
    private Color green = new Color(29f / 255f, 171f / 255f, 4f / 255f);
    private GameObject preview;
    private Point PreviewPoint;
    private bool reset = false;
    private GameObject gg;
    private int numberOfBoxes=0;
    private bool edit = false;
    private Point tooltipPoint;
    private string loginURL = "http://localhost/api/points/";
    private string saveURL = "http://localhost/api/points/add/";
    private string deleteURL = "http://localhost/api/points/del/";
    private Vector3 startViewPosition;
    private Quaternion startViewRotation;
    private ListOfPoints points;
    private WriteJSON writeJson;
    private bool gui = false;

    // Use this for initialization
    void Start()
    {

        btnReset.gameObject.SetActive(false);
        Button btn4 = btnCancelEdit.GetComponent<Button>();
        btn4.onClick.AddListener(CloseAndDelete);

        Button btn5 = btnSaveEdit.GetComponent<Button>();
        btn5.onClick.AddListener(SaveEdit);

        Button btn6 = btnCancelWarning.GetComponent<Button>();
        btn6.onClick.AddListener(Cancel);

        Button btn2 = btnReset.GetComponent<Button>();
        btn2.onClick.AddListener(Reset);

        Button btn7 = btnPreview.GetComponent<Button>();
        btn7.onClick.AddListener(Preview);

        Button btn8 = btnBack.GetComponent<Button>();
        btn8.onClick.AddListener(ResetCamera);
        btnBack.gameObject.SetActive(false);

        points = new ListOfPoints();

        Button btn3 = btnEditMode.GetComponent<Button>();
        btn3.onClick.AddListener(EditMode);
        
        StartCoroutine(Read());

        Button btn1 = btnCancel.GetComponent<Button>();
        btn1.onClick.AddListener(Close);

      
        GameObject JsonObject = GameObject.FindWithTag("MainCamera");
        if (JsonObject != null)
            writeJson = JsonObject.GetComponent<WriteJSON>();
        else
            Debug.Log("Cannot find MainCamera");

        panelEdit.SetActive(false);
        panelWarning.SetActive(false);
        tooltip.SetActive(false);
        panelPreview.SetActive(false);

        startViewPosition = myCamera.transform.position;
        startViewRotation = myCamera.transform.rotation;

        StartDrawGraph();
    }

    private void Close()
    {
        tooltip.SetActive(false);
    }

    void StartDrawGraph()
    {

        DrawLine(TopLeft.transform.position, MiddleLeft.transform.position, Color.black);
        DrawLine(MiddleLeft.transform.position, BottomLeft.transform.position, Color.black);

        DrawLine(MiddleLeft.transform.position, MiddleRight.transform.position, Color.green);
        DrawLine(Bound2Left.transform.position, Bound2Right.transform.position, Color.yellow);
        DrawLine(Bound5Left.transform.position, Bound5Right.transform.position, Color.red);
        DrawLine(Boundmin2Left.transform.position, Boundmin2Right.transform.position, Color.yellow);
        DrawLine(Boundmin5Left.transform.position, Boundmin5Right.transform.position, Color.red);
    }

    void Preview()
    {
        foreach (Transform child in LeftWallScreen.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        StartDrawGraph();
        DrawGraph(PreviewPoint);
        btnBack.gameObject.SetActive(true);
        PanelPreview();
        myCamera.transform.position = new Vector3(0f, 2.5f, -10f);
        myCamera.transform.rotation = new Quaternion();
        tooltip.SetActive(false);
    }
    
    void PanelPreview()
    {
        panelPreview.SetActive(true);
        paramPreview.text = PreviewPoint.ParameterName;
        int count = PreviewPoint.Values.Count;
        val1Preview.text = "Value " + (count - 3) + ":";
        val2Preview.text = "Value " + (count - 2) + ":";
        val3Preview.text = "Value " + (count - 1) + ":";
        val1prev.text = PreviewPoint.Values[count-3].Val.ToString();
        val2prev.text = PreviewPoint.Values[count-2].Val.ToString();
        val3prev.text = PreviewPoint.Values[count-1].Val.ToString();
    }

    void DrawGraph(Point p)
    {  
        Vector3 lastP = MiddleLeft.transform.position;
        Vector3 newP = new Vector3(lastP.x + 2f, lastP.y, lastP.z);

        foreach (Value v in p.Values)
         {
            lastP = DrawValue(v, lastP, newP);
            newP = new Vector3(lastP.x + 2f, lastP.y, lastP.z);
        }
    }

    private Vector3 DrawValue(Value newVal, Vector3 lastPosition, Vector3 newPosition)
    {
        float val = (float)newVal.Val;
        float x = (float)((10f * newVal.Val))/100f;
        float y = 2f + (4f - 2f) * x;
        GameObject g = (GameObject)Instantiate(coordinate, new Vector3(newPosition.x, y, newPosition.z), new Quaternion());
        g.transform.parent = LeftWallScreen.transform;

        if(val>-2f && val<2f)
        {
            DrawLine(lastPosition, new Vector3(newPosition.x, y, newPosition.z), Color.green);
        }
        else if((val<=-2f && val>-5f) || (val>=2f && val<5f))
        {
            DrawLine(lastPosition, new Vector3(newPosition.x, y, newPosition.z), Color.yellow);
        }
        else if(val>=5f || val<=-5f)
        {
            DrawLine(lastPosition, new Vector3(newPosition.x, y, newPosition.z), Color.red);
        }

//        DrawLine(lastPosition, new Vector3(newPosition.x, y, newPosition.z), Color.black);
        return new Vector3(newPosition.x, y, newPosition.z);
    }

    private void DrawLine(Vector3 start, Vector3 end, Color color)
    {
        GameObject myLine = new GameObject();
        myLine.transform.parent = LeftWallScreen.transform;
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        
        lr.material = material;
        lr.SetColors(color, color);
        lr.SetWidth(0.05f, 0.05f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        lr.material.color = color;
    }

    private void Cancel()
    {
        panelWarning.SetActive(false);
    }

    private void SaveEdit()
    {
        saveMode = true;
        Vector3 vv = transform.position;
     
        Quaternion q = new Quaternion();
        Point p = new Point(0, inputName.text, vv.x, vv.y, vv.z, inputPar.text);
        WriteToDB(p.Name, p.ParameterName, (float)p.X, (float)p.Y, (float)p.Z);
        inputPar.text = "";
        inputName.text = "";
        panelEdit.SetActive(false);
        tooltip.SetActive(false);
        saveMode = false;
        btnReset.gameObject.SetActive(true);
        reset = true;
    }

    private void CloseAndDelete()
    {
        Destroy(gg);
        panelEdit.SetActive(false);
    }

    IEnumerator Read()
    {
        foreach(Transform child in container.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        
        points = new ListOfPoints();
        List<Point> list_points;

        WWW www = new WWW(loginURL);
        yield return www;

        string json_text = "{\"points\":" + www.text + "}";
        Wrapper lll = JsonUtility.FromJson<Wrapper>(json_text);
        list_points = lll.points;

        pathText.text = list_points.Count.ToString();

        foreach (Point p in list_points)
        {
            points.points.Add(p);
        }

        LoadDrawPoints(points);
    }

    IEnumerator ReadOne(int id)
    {

        WWW www = new WWW(loginURL + id);
        yield return www;

        while (!www.isDone) { }

        Point p = JsonUtility.FromJson<Point>(www.text);
        
        DrawPoint(p);
    }

    void WriteToDB(string name, string param, float x, float y, float z)
    {
        WWWForm wwwf = new WWWForm();

        wwwf.AddField("Name", name);
        wwwf.AddField("X", x.ToString());
        wwwf.AddField("Y", y.ToString());
        wwwf.AddField("Z", z.ToString());
        wwwf.AddField("ParameterName", param);

        WWW www = new WWW(saveURL,wwwf);
        gg.transform.parent = container.transform;
        btnReset.gameObject.SetActive(true);

    }

    void Reset()
    {
        btnReset.gameObject.SetActive(false);
        tooltip.SetActive(false);
        reset = false;
        panelWarning.SetActive(false);
        StartCoroutine(Read());
    }

    void EditMode()
    {
        if(!edit)
        {
            Edittext.text = "Add : Mouse Left\nRotate : Mouse Right";
            edit = true;
        }
        else
        {
            Edittext.text = "Details : Mouse Left\nRotate : Mouse Right";
            edit = false;
        }
    }
    

    public void TaskOnClick(Point p)
    {
        PreviewPoint = p;
        if (reset)
        {
            panelWarning.SetActive(true);
        }
        else
        {
            tooltip.SetActive(true);
            DataView(p);
        }
    }

    void DataView(Point point)
    {
        IDtext.text = point.Id.ToString();
        Nametext.text = point.Name;
        ParamText.text = point.ParameterName;
        int count = point.Values.Count;
        TextVal1Preview.text = "Value " + (count - 3) + ":";
        TextVal2Preview.text = "Value " + (count - 2) + ":";
        TextVal3Preview.text = "Value " + (count - 1) + ":";
        TextVal1.text = point.Values[count - 3].Val.ToString();
        TextVal2.text = point.Values[count - 2].Val.ToString();
        TextVal3.text = point.Values[count-1].Val.ToString();
        tooltipPoint = point;
    }

    void LoadDrawPoints(ListOfPoints list)
    {
        foreach (Point p in list.points)
        {
            Vector3 vv = new Vector3((float)p.X, (float)p.Y, (float)p.Z);
            Quaternion q = new Quaternion();
            GameObject go = (GameObject)Instantiate(ppoint, vv, q);
            go.GetComponent<PointController>().point = p;
            go.transform.parent = container.transform;
        }
    }
    
    void DrawPoint(Point p)
    {
        Vector3 vv = new Vector3((float)p.X, (float)p.Y, (float)p.Z);
        Quaternion q = new Quaternion();
        GameObject go = (GameObject)Instantiate(ppoint, vv, q);
        go.GetComponent<PointController>().point = p;
        go.transform.parent = container.transform;
    }

    void DrawPoint(Transform t)
    {
        Vector3 vv = t.position;
        //vv.y += 10f;
        //vv.z -= 215f;
        pathText.text="transform: " + vv.x + " " + vv.y + " " + vv.z;
        Quaternion q = new Quaternion();
        Point p = new Point(0, "tacka", vv.x, vv.y, vv.z, "");
        gg = (GameObject)Instantiate(ppoint, vv, q);
        panelEdit.SetActive(true);
    }

    void ResetCamera()
    {
        btnBack.gameObject.SetActive(false);
        panelPreview.SetActive(false);
        myCamera.transform.position = startViewPosition;
        myCamera.transform.rotation = startViewRotation;
    }

    void Update()
    {
        if (edit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                Debug.Log(saveMode);
                if (!saveMode)
                {
                    bool returnBool = false;
                    foreach (Collider collider in plant1.GetComponentsInChildren<Collider>())
                    {
                        if (collider.Raycast(ray, out hit, Mathf.Infinity))
                        {
                            EditMode();
                            transform.position = hit.point;
                            Vector3 vv = transform.position;
                            vv.y += 10f;
                            vv.z += 70f;
                            Debug.Log(vv);
                            transform.position = vv;
                            DrawPoint(transform);
                            returnBool = true;
                            return;
                        }
                    }

                    if (!returnBool)
                    {
                        foreach (Collider collider in plant2.GetComponentsInChildren<Collider>())
                        {
                            if (collider.Raycast(ray, out hit, Mathf.Infinity))
                            {
                                EditMode();
                                transform.position = hit.point;
                                Vector3 vv = transform.position;
                                vv.x += 900f;
                                vv.z += 200f;
                                DrawPoint(transform);
                                return;
                            }
                        }
                    }
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetCamera();
        }
    }



}


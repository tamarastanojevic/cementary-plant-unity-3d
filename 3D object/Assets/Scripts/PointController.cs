﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class PointController : MonoBehaviour {

    public Transform t;
    public Point point;
    public Renderer vMesh;

    private float time = 0.0f;
    public float pausePeriod = 3.5f;
    private Color red = new Color(242f / 255f, 36f / 255f, 33f / 255f);
    private Color yellow = new Color(219f / 255f, 209f / 255f, 2f / 255f);
    private Color green = new Color(29f / 255f, 171f / 255f, 4f / 255f);
    private Controller controller;
    private string loginURL = "http://localhost/api/points/";
    private int oldCount;
    private int newCount;

    void Start()
    {
        GameObject ControllerObject = GameObject.FindWithTag("GameController");
        if (ControllerObject != null)
            controller = ControllerObject.GetComponent<Controller>();
        else
            Debug.Log("Cannot find GameController");
        ChangeColor();
    }

	void OnMouseDown()
    {
        if(Input.GetKey("mouse 0"))
        {
            
            Debug.Log("id: " + point.Id + ", name: " + point.Name + ", x: " + point.X + ", y: " + point.Y + ", z: " + point.Z);

            controller.TaskOnClick(point);
        }
    }

    void Update()
    {
        time += Time.deltaTime;

        if (time >= pausePeriod)
        {
            time = 0.0f;

            Prepare();
        }           
    }

    IEnumerator ReadOne(int id)
    {
        WWW www = new WWW(loginURL + id);
        yield return www;

        while (!www.isDone) { }

        oldCount = point.Values.Count;
        point = JsonUtility.FromJson<Point>(www.text);
        newCount = point.Values.Count;
    }

    void Prepare()
    {
        StartCoroutine(ReadOne(point.Id));


        if (oldCount < newCount)
        {
            for (int i = oldCount; i < newCount; i++)
            {
                if (point.Values[i].Val > -2f && point.Values[i].Val < 2)
                {
                    Debug.Log("id: " + point.Id + " ---> zeleno <--- " + point.Values[i].Val);
                    vMesh.material.color = green;
                }
                else if ((point.Values[i].Val >= 2f && point.Values[i].Val < 5f) || (point.Values[i].Val <= -2f && point.Values[i].Val > -5f))
                {
                    Debug.Log("id: " + point.Id + "---> zuto <--- " + point.Values[i].Val);
                    vMesh.material.color = yellow;
                }
                else if (point.Values[i].Val >= 5f || point.Values[i].Val <= -5f)
                {
                    Debug.Log("id: " + point.Id + "---> crveno <--- " + point.Values[i].Val);
                    vMesh.material.color = red;
                }
            }

        }
    }

    void ChangeColor()
    {
        int count = point.Values.Count-1;
        Debug.Log("count u point values je : " + count);
        if (count>0)
        {
            if (point.Values[count].Val > -2f && point.Values[count].Val < 2)
            {
                vMesh.material.color = green;
            }
            else if ((point.Values[count].Val >= 2f && point.Values[count].Val < 5f) || (point.Values[count].Val <= -2f && point.Values[count].Val > -5f))
            {
                vMesh.material.color = yellow;
            }
            else if (point.Values[count].Val >= 5f || point.Values[count].Val <= -5f)
            {
                vMesh.material.color = red;
            }
        }
    }
}

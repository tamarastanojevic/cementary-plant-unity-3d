﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Value
{
    public int Id;
    public double Val;
    public int PointID;
    public Point Point;

    public Value()
    {

    }

    public Value(int id, double v, int pid, Point p)
    {
        this.Id = id;
        this.Val = v;
        this.PointID = pid;
        this.Point = p;
    }


}
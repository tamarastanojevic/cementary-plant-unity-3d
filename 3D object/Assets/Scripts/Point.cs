﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Point
{
    public List<Value> Values;
    public int Id;
    public string Name;
    public double X;
    public double Y;
    public double Z;
    public string ParameterName;

    public Point()
    {
        Values = new List<Value>();
    }

    public Point(int id, string n, double x, double y, double z, string Param)
    {
        Values = new List<Value>();
        this.Id = id;
        this.X = x;
        this.Y = y;
        this.Z = z;
        this.Name = n;
        this.ParameterName = Param;
    }

}

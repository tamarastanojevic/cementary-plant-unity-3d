﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public class ListOfPoints
{
    public List<Point> points;

    public ListOfPoints()
    {
        points = new List<Point>();
    }

    public void Add(Point t)
    {
        points.Add(t);
    }
    
}

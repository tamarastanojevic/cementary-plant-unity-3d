﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PointDataAccess;
using System.Net.Http.Headers;
using System.Collections;

namespace Point.Controllers
{
    public class PointsController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/points/")]
        public IEnumerable GetPoints()
        {
            using (Entities entities = new Entities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                IEnumerable<PointDataAccess.Point> points = entities.Points.ToList();
                IEnumerable<PointDataAccess.Value> values = entities.Values.ToList();
                IEnumerable<PointDataAccess.Value> values1 = values.OrderBy(e => e.PointID);
                IList<PointDataAccess.Value> lvalue = values1.ToList();
                

                foreach (PointDataAccess.Point p in points)
                {
                    foreach (PointDataAccess.Value v in lvalue)
                    {
                        if (p.Id == v.PointID)
                        {
                            p.Values.Add(v);
                        }
                    }
                }

                return points;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/points/{x}")]
        public PointDataAccess.Point Get(int x)
        {
            using (Entities entities = new Entities())
            {
                entities.Configuration.LazyLoadingEnabled = false;
                PointDataAccess.Point p = entities.Points.FirstOrDefault(e => e.Id == x);
                IEnumerable<PointDataAccess.Value> values = entities.Values.ToList();
                IEnumerable<PointDataAccess.Value> values1 = values.OrderBy(e => e.PointID);
                IList<PointDataAccess.Value> lvalue = values1.ToList();

                foreach (PointDataAccess.Value v in lvalue)
                {
                    if (p.Id == v.PointID)
                    {
                        p.Values.Add(v);
                    }
                }

                return p;
                
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/points/add")]
        public string Post([FromBody] PointDataAccess.Point p)
        {
            using (Entities entities = new Entities())
            {
                string s="";
                PointDataAccess.Point aPoint = new PointDataAccess.Point();
                PointDataAccess.Value aValue; 

                aPoint.Id = p.Id;
                aPoint.Name = p.Name;
                aPoint.X = p.X;
                aPoint.Y = p.Y;
                aPoint.Z = p.Z;
                aPoint.ParameterName = p.ParameterName;

                Random r = new Random();
               
                int i;
                for (i=0; i < 3; i++)
                {
                    double sample = Math.Round(r.NextDouble(), 5);
                    int range = r.Next(-10,10);
                    double f = sample + range;

                    double ff = Math.Round(f, 5);

                    s += "range: " + range + ", sample: " + sample + ", f: " + f + ", ff: "+ff+"\n";

                    
                    aValue = new PointDataAccess.Value();

                    aValue.PointID = p.Id;
                    aValue.Point = aPoint;
                    aValue.Val = ff;
                    entities.Values.Add(aValue);
                    aPoint.Values.Add(aValue);
                }

                entities.Points.Add(aPoint);

                int count1 = entities.Points.Count();

                entities.SaveChanges();

                IEnumerable<PointDataAccess.Point> v= entities.Points.ToList();
                int count2 = v.Count();

                return "ok + val: "+s;
                
            }

            return "Not";

        }
        
        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/points/del/{x}")]
        public string Delete(int x)
        {
            string s = "";
            using (Entities entities = new Entities())
            {

                entities.Configuration.LazyLoadingEnabled = false;
                PointDataAccess.Point temp = new PointDataAccess.Point();
                
                foreach (PointDataAccess.Point p in entities.Points)
                {
                    if (p.Id == x)
                    {
                        s = x.ToString();
                        temp = p;
                    }
                }

                if (temp != null)
                {
                    temp.Values = null;
                    entities.Points.Remove(temp);

                    foreach(PointDataAccess.Value v in entities.Values)
                    {
                        if(v.PointID==x)
                        {
                            entities.Values.Remove(v);
                        }
                    }
                    entities.SaveChanges();
                    s = entities.Points.Count().ToString()+ " "+ entities.Values.Count();
                }
                else
                    s = "null";
                return s;

            }

            return s="Not deleted";

        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/value/")]
        public IEnumerable<PointDataAccess.Value> GetValues()
        {
            using (Entities entities = new Entities())
            {
                entities.Configuration.LazyLoadingEnabled = false;
                return entities.Values.ToList();
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/value/{x}")]
        public PointDataAccess.Value GetValue(int x)
        {
            using (Entities entities = new Entities())
            {
                entities.Configuration.LazyLoadingEnabled = false;
                return entities.Values.FirstOrDefault(e=>e.Id==x);
            }
        }
        
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/value/add")]
        public string PostValue([FromBody] PointDataAccess.Value val)
        {
            using (Entities entities = new Entities())
            {
                entities.Configuration.LazyLoadingEnabled = false;

                PointDataAccess.Value a = new PointDataAccess.Value();
                PointDataAccess.Point p=null;
                foreach(PointDataAccess.Point point in entities.Points)
                {
                    if(point.Id==val.PointID)
                    {
                        a.Point = point;
                        a.PointID = val.PointID;
                        a.Val = val.Val;
                    }
                }
                

                entities.Values.Add(a);

                int count1 = entities.Values.Count();

                foreach (PointDataAccess.Point point in entities.Points)
                {
                    if (point.Id == val.PointID)
                    {
                        point.Values.Add(a);
                        p = point;
                    }
                }

                entities.SaveChanges();

                IEnumerable<PointDataAccess.Value> v = entities.Values.ToList();
                int count2 = v.Count();

                return "ok + "+count2+ " p-v: "+p.Values.Count();

            }

            return "Not";

        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/value/del/{x}")]
        public string DeleteValue(int x)
        {
            string s = "";
            using (Entities entities = new Entities())
            {

                entities.Configuration.LazyLoadingEnabled = false;
                foreach (PointDataAccess.Value v in entities.Values)
                {
                    if (v.Id == x)
                    {
                        entities.Values.Remove(v);
                    }
                }
                entities.SaveChanges();
                s = entities.Values.Count().ToString();
                return s;
            }

            return s = "Not deleted";

        }

    }
}